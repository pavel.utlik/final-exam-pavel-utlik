<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Apartment;
use AppBundle\Form\ApartmentCreate;
use AppBundle\Repository\ApartmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApartmentController extends Controller
{

    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;


    public function __construct(ApartmentRepository $apartmentRepository)
    {
        $this->apartmentRepository = $apartmentRepository;
    }


    /**
     * @Route("/admin/{id}", methods={"GET"}, name="show-action",
     * requirements={"id"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {

        $response = $this->apartmentRepository->findOneById($id);

        if($response == null){
            return new Response("This page not found!", 404);
        }

        return $this->render('default/apartment/show.html.twig', [
            'string' => $response
        ]);

    }


    /**
     * @Route("/admin/create/new", methods={"GET", "POST"}, name="create-action-post-first")
     * requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function CreateAction(Request $request, $id = 0)
    {

        define('second_route', 'create-action-apartment-second');

        $routeName = $request->get('_route');

        if ($routeName != second_route) {

            $apartment = new Apartment();

            $form = $this->createForm(ApartmentCreate::class, $apartment);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $apartment = $form->getData();

                $this->apartmentRepository->save($apartment);

                return $this->redirectToRoute('index-action-apartment', [
                    'page' => 'something',
                    'pageId' => 1
                ]);

            }

            return $this->render('default/apartment/create.html.twig', [
                'form' => $form->createView()
            ]);
        }
        $apartment = $this->apartmentRepository->findOneById($id);

        $form = $this->createForm(ApartmentCreate::class, $apartment);

        $form->handleRequest($request);
        return $this->render('default/Apartment/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}