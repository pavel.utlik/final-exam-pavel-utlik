<?php


namespace AppBundle\Repository;

use AppBundle\Entity\Apartment;
use Doctrine\ORM\EntityManagerInterface;


class ApartmentRepository
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var
     */
    private $repository;


    /**
     * ApartmentRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Apartment::class);
        $this->entityManager = $entityManager;
    }
    /**
     * @param $apartmentId
     * @return Apartment|null
     */
    public function findOneById($apartmentId)
    {
        return $this->repository->find($apartmentId);
    }
    /**
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }
    /**
     * @param Apartment $apartment
     */
    public function save(Apartment $apartment)
    {
        $this->entityManager->persist($apartment);
        $this->entityManager->flush();
    }
}