<?php


namespace AppBundle\Form;


use AppBundle\Entity\Apartment;
use Symfony\Component\Config\Definition\IntegerNode;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ApartmentCreate extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Title', TextType::class, [
                'required' => true
            ])
            ->add('zipCode', IntegerType::class,[
                'required' => true
            ])
            ->add('City', TextType::class,[
                'required' => true
            ])
            ->add('Street', TextType::class,[
            'required' => true
            ])
            ->add('Building', IntegerType::class, [
                'required' => true
            ])
            ->add('ApartmentNumber', IntegerType::class, [
                'required' => true
            ])

            ->add('Save', SubmitType::class);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Apartment::class,
        ]);
    }
}