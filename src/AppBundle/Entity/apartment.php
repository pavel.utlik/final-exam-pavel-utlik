<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class apartment
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="apartment")
*/
class Apartment
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min=7, minMessage="Title field can contain a minimum 7 symbols")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="zipCode", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min=7, minMessage="zipCode field can contain a minimum 7 symbols")
     */
    private $zipCode;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=150)
     * @Assert\NotBlank()
     * @Assert\Length(min=3, minMessage="Password field can contain a minimum 3 symbols")
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="street", type="string", length=200)
     * @Assert\NotBlank()
     * @Assert\Length(min=4, minMessage="Street field can contain a minimum 4 symbols")
     */
    private $street;

    /**
     * @var string
     * @ORM\Column(name="building", type="string", length=200)
     * @Assert\NotBlank()
     * @Assert\Length(min=7, minMessage="building field can contain a minimum 7 symbols")
     */
    private $building;

    /**
     * @var string
     * @ORM\Column(name="apartmentNumber", type="string", length=200)
     * @Assert\NotBlank()
     * @Assert\Length(min=1, minMessage="Apartment Number field can contain a minimum 1 symbols")
     */
    private $apartmentNumber;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Apartment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return Apartment
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Apartment
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Apartment
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set building
     *
     * @param string $building
     *
     * @return Apartment
     */
    public function setBuilding($building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set apartmentNumber
     *
     * @param string $apartmentNumber
     *
     * @return Apartment
     */
    public function setApartmentNumber($apartmentNumber)
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }

    /**
     * Get apartmentNumber
     *
     * @return string
     */
    public function getApartmentNumber()
    {
        return $this->apartmentNumber;
    }
}
